package com.datagear.lm.models;

import lombok.*;
@AllArgsConstructor
@Getter
@Setter
public class Credentials {
    private String userName;
    private String userPassword;
}
