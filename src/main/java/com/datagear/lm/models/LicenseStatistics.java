package com.datagear.lm.models;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LicenseStatistics {
    private Long activatedLicencesCounter;
    private Long deactivatedLicencesCounter;
    private Long licensesThatNeedsRenewalCounter;
}
