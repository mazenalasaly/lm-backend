package com.datagear.lm.models;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LicensesPerBank {

    private String bankName;
    private int lcencesPerBankCounter;
}
