package com.datagear.lm.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "licenses")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class License {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    @Column(name="licenseCode")
    private String licenseCode;

    @NotBlank
    @Column(name="dateCreated")
    private Date dateCreated;

    @NotBlank
    @Column(name="dateExpired")
    private Date dateExpired;

    @NotBlank
    @Column(name="createdy")
    private Long createdBy;


    @NotBlank
    @Column(name="isActivated")
    @JsonProperty("isActivated")
    private boolean isActivated;

    @Lob
    @Column(name="licenseFile")
    private byte[] licenseFile;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bank_id", nullable = false)
    @JsonBackReference(value="bank-licenses")
    private Bank bank = new Bank();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "application_id", nullable = false)
    @JsonBackReference(value="application-licenses")
    private Application application = new Application();
}
