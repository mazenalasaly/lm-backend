package com.datagear.lm.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    @Column(name="userName")
    private String userName;

    @NotBlank
    @Size(max = 20)
    @Column(name="userPassword")
    private String userPassword;

    @NotBlank
    @Column(name="dateExpired")
    private String userEmail;

    @NotBlank
    @Column(name="dateOfBirth")
    private Date dateOfBirth;

    private boolean isActivate;


}
