package com.datagear.lm.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "banks")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "bankName", unique=true)
    private String bankName;

    @Column(name = "bankCode", unique=true)
    private String bankCode;

//    @ManyToMany
//    @JoinTable(
//            name = "bank_Applications",
//            joinColumns = @JoinColumn(name = "bank_id"),
//            inverseJoinColumns = @JoinColumn(name = "application_id"))
//    List<Application> applications;

    @OneToMany(mappedBy = "bank", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JsonManagedReference(value="bank-licenses")
    private List<License> licenses = new ArrayList<>();


//    public void addApplication(Application application) {
//        this.applications.add(application);
//        application.getBanks().add(this);
//    }
//    public void removeApplication(Application application) {
//        this.getApplications().remove(application);
//        application.getBanks().remove(this);
//    }


}
