package com.datagear.lm.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "applications")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Application {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "applicationName", unique=true)
    private String applicationName;
    @Column(name = "applicationDetails")
    private String applicationDetails;

//    @ManyToMany(mappedBy = "applications")
//    @JsonIgnore
//    List<Bank> banks;

    @OneToMany(mappedBy = "bank", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JsonManagedReference(value="application-licenses")
    private List<License> licenses = new ArrayList<>();
}
