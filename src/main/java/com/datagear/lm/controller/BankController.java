package com.datagear.lm.controller;


import com.datagear.lm.models.Bank;
import com.datagear.lm.models.License;
import com.datagear.lm.service.BankService;
import com.datagear.lm.service.LicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bank")
@CrossOrigin(origins = "*")
public class BankController {

    @Autowired
    private BankService bankService;
    @Autowired
    private LicenseService licenseService;

    @GetMapping("/banks")
    public ResponseEntity<List<Bank>> listBanks() {
        return bankService.listBanks();
    }

    @GetMapping("/{bankId}/licenses")
    public ResponseEntity<List<License>> listBankLicensesByBankId(@PathVariable("bankId") long bankId) {
        return licenseService.listLicensesByBankId(bankId);
    }

    @GetMapping("/banks/names")
    public ResponseEntity<List<String>> listBanksNames() {
        return bankService.listBanksNames();
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<Bank> getBankById(@PathVariable("id") long id) {
        return bankService.getBankById(id);
    }

    @PostMapping("/create")
    public ResponseEntity<Bank> createBank(@RequestBody Bank bank) {
        return bankService.createBank(bank);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Bank> updateBank(@PathVariable("id") long id, @RequestBody Bank bank) {
        return bankService.updateBank(id, bank);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> deleteBank(@PathVariable("id") long id) {
        return bankService.deleteBank(id);
    }


//    @PutMapping("/{bankId}/addApplication/{applicationId}")
//    public ResponseEntity<Bank> addApplication(@PathVariable("bankId") long bankId, @PathVariable("applicationId") long applicationId,@RequestBody Bank bank) {
//        return bankService.addApplication(bankId, bank, applicationId);
//    }

//    @GetMapping("/banks/banksStatistics")
//    public ResponseEntity<LicenseStatistics> getAllLicensesStatistics() {
//        return licenseService.getAllLicensesStatistics();
//    }
}