package com.datagear.lm.controller;


import com.datagear.lm.models.License;
import com.datagear.lm.models.LicenseStatistics;
import com.datagear.lm.service.LicenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/license")
@CrossOrigin(origins = "*")
public class LicenseController {

    @Autowired
    private LicenseService licenseService;
    Logger log = LoggerFactory.getLogger(LicenseController.class);

    @GetMapping("/licenses")
    public ResponseEntity<List<License>> listLicenses() {
       return licenseService.listLicenses();
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<License> getLicenseById(@PathVariable("id") long id) {
        return licenseService.findLicenseById(id);
    }


    @PostMapping("/create")
    public ResponseEntity<License> createLicense(@RequestBody License license) {
        return licenseService.createLicense(license);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<License> updateLicense(@PathVariable("id") long id, @RequestBody License license) {
       return licenseService.updateLicense(id, license);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Boolean> deleteLicense(@PathVariable("id") long id) {
       return licenseService.deleteLicense(id);
    }



    @GetMapping("/licensesCodes")
    public ResponseEntity<List<String>>  getLicensesCodes() {
        return licenseService.getLicensesCodes();
    }

    @GetMapping("/licenses/activated/list")
    public ResponseEntity<List<License>> getAllActivatedLicenses() {
        return licenseService.findActivatedLicenses();
    }

    @GetMapping("/licenses/deactivated/list")
    public ResponseEntity<List<License>> getAllDeactivatedLicenses() {
        return licenseService.findDeactivatedLicenses();
    }

    @GetMapping("/licenses/renewal/list")
    public ResponseEntity<List<License>> getAllLicensesThatNeedsRenewal() {
        return licenseService.findAllLicensesThatNeedsRenewal();
    }

    @GetMapping("/licenses/licensesStatistics")
    public ResponseEntity<LicenseStatistics> getAllLicensesStatistics() {
        return licenseService.getAllLicensesStatistics();
    }

    @GetMapping("/licenses/licensesPerBank")
    public List<java.util.ArrayList<Object>> getAllLicensesPerBank() {
        return licenseService.getAllLicensesPerBank();
    }



    @GetMapping("/{id}/download")
    public ResponseEntity<byte[]> getLicenseFileById(@PathVariable("id") long id) {
        return licenseService.findLicenseFileById(id);
    }

    @GetMapping("/licenses/licensesPerBankStatistics")
    public ResponseEntity<Map<String,Long>> getAllLicensesPerBankStatistics() {
        return licenseService.getAllLicensesPerBankStatistics();
    }

    @GetMapping("/licenses/renwalLicenses")
    public ResponseEntity<List<License>> getAllRenewalLicenses() {
        return licenseService.findAllLicensesThatNeedsRenewal();
    }





}
