package com.datagear.lm.controller;


import com.datagear.lm.models.Credentials;
import com.datagear.lm.models.User;
import com.datagear.lm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> listUsers() {
        return userService.listUsers();
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
        return userService.findUserById(id);
    }

    @PostMapping("/auth")
    public ResponseEntity<User> authenticateUser(@RequestBody Credentials credentials) {
        return userService.authenticateUser(credentials);
    }

    @PostMapping("/create")
    public ResponseEntity<User> createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User user) {
        return userService.updateUser(id, user);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable("id") long id) {
        return userService.deleteUser(id);
    }

}
