package com.datagear.lm.controller;


import com.datagear.lm.models.Application;
import com.datagear.lm.models.License;
import com.datagear.lm.service.ApplicationService;
import com.datagear.lm.service.LicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/application")
@CrossOrigin(origins = "*")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private LicenseService licenseService;

    @GetMapping("/applications")
    public ResponseEntity<List<Application>> listApplications() {
        return applicationService.listApplications();
    }

    @GetMapping("/{applicationId}/licenses")
    public ResponseEntity<List<License>> listLicensesByBankId(@PathVariable("applicationId") long applicationId) {
        return licenseService.listLicensesByApplicationId(applicationId);
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<Application> getApplicationById(@PathVariable("id") long id) {
        return applicationService.findApplication(id);
    }

    @GetMapping("/{id}/banks")
    public ResponseEntity<Application> getApplicationLicensesById(@PathVariable("id") long id) {
        return applicationService.getApplicationBanks(id);
    }

    @PostMapping("/create")
    public ResponseEntity<Application> createApplication(@RequestBody Application application) {
        return applicationService.createApplication(application);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Application> updateApplication(@PathVariable("id") long id, @RequestBody Application application) {
        return applicationService.updateApplication(id, application);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> deleteApplication(@PathVariable("id") long id) {
        return applicationService.deleteApplication(id);
    }

}