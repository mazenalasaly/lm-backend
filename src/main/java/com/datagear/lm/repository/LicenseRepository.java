package com.datagear.lm.repository;

import com.datagear.lm.models.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LicenseRepository extends JpaRepository<License,Long> {

    List<License> findAllByBank_Id(long bankId);
    int countAllByBank_Id(long bankId);
    List<License> findAllByApplication_Id(long applicationId);
    List<License> findAllByIsActivatedTrue();
    List<License> findAllByIsActivatedFalse();
    @Query( value = "SELECT * FROM   licenses l "+
            "WHERE DATEDIFF(day, GETDATE(), l.date_expired )  < ?1", nativeQuery = true)
    List<License> findAllByDateExpiredLessThanEqual(int numberOfDaysToNotifyRenewal);
    Long countLicensesByBankIs(Long bankId);
    Long countByIsActivatedTrue( );
    Long countByIsActivatedFalse();
    @Query( value = "SELECT Count(*) FROM   licenses l "+
            "WHERE DATEDIFF(day, GETDATE(), l.date_expired )  < ?1", nativeQuery = true)
    Long countByDateExpiredLessThanEqual(int numberOfDaysToNotifyRenewal);
    @Query( value = "SELECT l.license_code  FROM licenses l", nativeQuery = true)
    List<String> getLicensesCodes();


}
