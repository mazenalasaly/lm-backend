package com.datagear.lm.repository;


import com.datagear.lm.models.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BankRepository extends JpaRepository<Bank, Long> {
    @Query( value = "SELECT b.bank_name  FROM banks b", nativeQuery = true)
    List<String> getAllBanksNames();
    @Query( value = "SELECT b.id  FROM banks b ", nativeQuery = true)
    List<Integer> getAllId();
    @Query( value = "SELECT count(*)  FROM banks b", nativeQuery = true)
    int countAll();
    @Query( value = "SELECT b.bank_name  FROM banks b "+
            "WHERE b.id = ?1", nativeQuery = true)
    String getBankNameById(int bankId);

}
