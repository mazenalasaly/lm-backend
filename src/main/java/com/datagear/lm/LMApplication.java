package com.datagear.lm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LMApplication {

    public static void main(String[] args) {
        SpringApplication.run(LMApplication.class, args);
    }

}
