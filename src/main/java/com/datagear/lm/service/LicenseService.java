package com.datagear.lm.service;

import com.datagear.lm.models.Bank;
import com.datagear.lm.models.License;
import com.datagear.lm.models.LicenseStatistics;
import com.datagear.lm.repository.ApplicationRepository;
import com.datagear.lm.repository.BankRepository;
import com.datagear.lm.repository.LicenseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LicenseService {
    @Autowired
    private LicenseRepository licenseRepository;
    @Autowired
    private BankRepository bankRepository;
    @Autowired
    private ApplicationRepository applicationRepository;
    @Value("${NumberOfDaysToNotifyRenewal}")
    private int numberOfDaysToNotifyRenewal;

    Logger log = LoggerFactory.getLogger(LicenseService.class);

    public ResponseEntity<License> createLicense (License license){
        try {
            License savedLicense = licenseRepository.save(license);
            return new ResponseEntity<>(savedLicense, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public ResponseEntity<License> updateLicense(long id, License license) {
        Optional<License> licenseData = licenseRepository.findById(id);

        if (licenseData.isPresent()) {
            License upadtedLicense = licenseData.get();
//            upadtedLicense.setLicenseCode(license.getLicenseCode());
//            upadtedLicense.setDateCreated(license.getDateCreated());
            upadtedLicense.setActivated(license.isActivated());
//            upadtedLicense.setDateExpired(license.getDateExpired());
//            upadtedLicense.setLicenseFile(license.getLicenseFile());
//            upadtedLicense.setBank(license.getBank());
//            upadtedLicense.setApplication(license.getApplication());
            return new ResponseEntity<>(licenseRepository.save(upadtedLicense), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Boolean> deleteLicense(long id) {
        try {
            licenseRepository.deleteById(id);
            return new ResponseEntity<>(true,HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<License> findLicenseById(long id) {
        Optional<License> license = licenseRepository.findById(id);
        if (license.isPresent()) {
            return new ResponseEntity<>(license.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    public ResponseEntity<List<License>> listLicenses() {
        try {
            List<License> licenses = licenseRepository.findAll();
            return new ResponseEntity<>(licenses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<License>> listLicensesByBankId(long bankId) {
        try {
            List<License> licenses = licenseRepository.findAllByBank_Id(bankId);
            return new ResponseEntity<>(licenses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<License>> listLicensesByApplicationId(long applicationId) {
        try {
            List<License> licenses = licenseRepository.findAllByApplication_Id(applicationId);
            return new ResponseEntity<>(licenses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<License>> findActivatedLicenses() {
        try {
            List<License> licenses = licenseRepository.findAllByIsActivatedTrue();
            return new ResponseEntity<>(licenses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<License>> findDeactivatedLicenses() {
        try {
            List<License> licenses = licenseRepository.findAllByIsActivatedFalse();
            return new ResponseEntity<>(licenses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<License>> findAllLicensesThatNeedsRenewal() {

        try {
            List<License> licensesThatNeedsRenewal = licenseRepository.findAllByDateExpiredLessThanEqual(numberOfDaysToNotifyRenewal);
            return new ResponseEntity<>(licensesThatNeedsRenewal, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<LicenseStatistics> getAllLicensesStatistics() {
        try {
            long activatedLicensesCounter = licenseRepository.countByIsActivatedTrue();
            long deactivatedLicensesCounter = licenseRepository.countByIsActivatedFalse();
            long licensesThatNeedsRenewalCounter = licenseRepository.countByDateExpiredLessThanEqual(numberOfDaysToNotifyRenewal);
            LicenseStatistics licenseStatistics = new LicenseStatistics();
            licenseStatistics.setActivatedLicencesCounter(activatedLicensesCounter);
            licenseStatistics.setDeactivatedLicencesCounter(deactivatedLicensesCounter);
            licenseStatistics.setLicensesThatNeedsRenewalCounter(licensesThatNeedsRenewalCounter);
            return new ResponseEntity<LicenseStatistics>(licenseStatistics, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<String>> getLicensesCodes() {
        try {
            List<String> licenceCodes = licenseRepository.getLicensesCodes();
            return new ResponseEntity<>(licenceCodes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<byte[]> findLicenseFileById(long id) {
        Optional<License> license = licenseRepository.findById(id);
        if (license.isPresent()) {
            byte [] licenseFile = license.get().getLicenseFile();
            return new ResponseEntity<byte[]>(licenseFile, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    public ResponseEntity<Map<String, Long>> getAllLicensesPerBankStatistics() {
        try {
            Map <String, Long>  LicensesPerBankStatistics = new HashMap<String, Long>();
            List<Bank> allBanks = bankRepository.findAll();
            Long bankLicensesCounter;
            for (Bank bank : allBanks){
                bankLicensesCounter=licenseRepository.countLicensesByBankIs(bank.getId());
                LicensesPerBankStatistics.put(bank.getBankName(), bankLicensesCounter ) ;
                log.info("bank name: " + bank.getBankName() );
                log.info("bank Counter: "+ bankLicensesCounter );
            }
            return new ResponseEntity<>(LicensesPerBankStatistics, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public List<ArrayList<Object>> getAllLicensesPerBank() {
        int numberOfBanks = bankRepository.countAll();
        List<Integer> BankIds = bankRepository.getAllId();
        log.info("BankIds "+ BankIds);
        List<ArrayList<Object>> allLicensesPerBanks = new ArrayList<ArrayList<Object>>(numberOfBanks);
        String bankName;
        for ( int  bankId : BankIds ){
            ArrayList<Object> allLicensesPerBank= new ArrayList<Object>();
            log.info("allLicensesPerBank " + allLicensesPerBank.isEmpty());
            log.info("bankId " + bankId);
            bankName= bankRepository.getBankNameById(bankId);
            log.info("bankName " + bankName);
            allLicensesPerBank.add(bankRepository.getBankNameById(bankId));
            log.info("allLicensesPerBank " + allLicensesPerBank.get(0));
            allLicensesPerBank.add(licenseRepository.countAllByBank_Id(bankId));
            allLicensesPerBanks.add(allLicensesPerBank);
        }

        return allLicensesPerBanks;
       
    }


}
