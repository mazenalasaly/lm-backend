package com.datagear.lm.service;

import com.datagear.lm.models.Application;
import com.datagear.lm.repository.ApplicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ApplicationService {
    @Autowired
    private ApplicationRepository applicationRepository;
    Logger log = LoggerFactory.getLogger(ApplicationService.class);

    public ResponseEntity<Application> createApplication (Application application){
        try {
            Application saveApplication = applicationRepository.save(application);
            return new ResponseEntity<>(saveApplication, HttpStatus.CREATED);
        } catch (Exception e) {
            log.info(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Application>> listApplications() {
        try {
            List<Application> applications = applicationRepository.findAll();
            return new ResponseEntity<>(applications, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public ResponseEntity<HttpStatus> deleteApplication(long id) {
        try {
            applicationRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Application> updateApplication(long id, Application application) {
        Optional<Application> applicationData = applicationRepository.findById(id);

        if (applicationData.isPresent()) {
            Application updatedApplication = applicationData.get();
            updatedApplication.setApplicationName(application.getApplicationName());
            updatedApplication.setApplicationDetails(application.getApplicationDetails());
            return new ResponseEntity<>(applicationRepository.save(updatedApplication), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Application> getApplicationBanks(long id) {
        Optional<Application> application = applicationRepository.findById(id);
        if (application.isPresent()) {
            return new ResponseEntity<>(application.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Application> findApplication(long id) {
        Optional<Application> application = applicationRepository.findById(id);
        if (application.isPresent()) {
            return new ResponseEntity<>(application.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
