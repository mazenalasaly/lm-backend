package com.datagear.lm.service;

import com.datagear.lm.models.Credentials;
import com.datagear.lm.models.User;
import com.datagear.lm.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    Logger log = LoggerFactory.getLogger(UserService.class);

    public ResponseEntity<User> createUser (User user){
        try {
            User savedUser = userRepository.save(user);
            return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public Optional<User> findById(long id) {
        return userRepository.findById(id);
    }

    public ResponseEntity<User> updateUser(long id, User user) {
        Optional<User> userData = userRepository.findById(id);

        if (userData.isPresent()) {
            User upadtedUser = userData.get();
            upadtedUser.setUserName(user.getUserName());
            upadtedUser.setUserPassword(user.getUserPassword());
            upadtedUser.setUserEmail(user.getUserEmail());
            upadtedUser.setDateOfBirth(user.getDateOfBirth());
            upadtedUser.setActivate(user.isActivate());
            return new ResponseEntity<>(userRepository.save(upadtedUser), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Boolean> deleteUser(long id) {
        try {
            userRepository.deleteById(id);
            return new ResponseEntity<>(true,HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<User> findUserById(long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<List<User>> listUsers() {
        try {
            List<User> users = userRepository.findAll();
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<User> authenticateUser(Credentials credentials) {
        Optional<User> user = userRepository.findByUserNameAndUserPassword(credentials.getUserName(), credentials.getUserPassword());
        if (user.isPresent()) {
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
