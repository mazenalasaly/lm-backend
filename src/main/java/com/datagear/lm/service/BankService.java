package com.datagear.lm.service;

import com.datagear.lm.models.Bank;
import com.datagear.lm.repository.ApplicationRepository;
import com.datagear.lm.repository.BankRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BankService {
    @Autowired
    private BankRepository bankRepository;
    @Autowired
    private ApplicationRepository applicationRepository;
    Logger log = LoggerFactory.getLogger(BankService.class);

    public ResponseEntity<Bank> createBank (Bank bank){
        try {
            Bank saveBank = bankRepository.save(bank);
            return new ResponseEntity<>(saveBank, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Bank>> listBanks() {
        try {
            List<Bank> banks = bankRepository.findAll();
            return new ResponseEntity<>(banks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public ResponseEntity<HttpStatus> deleteBank(long id) {
        try {
            bankRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Bank> updateBank(long id, Bank bank) {
        Optional<Bank> bankData = bankRepository.findById(id);

        if (bankData.isPresent()) {
            Bank updatedBank = bankData.get();
            updatedBank.setBankName(bank.getBankName());
            updatedBank.setBankCode(bank.getBankCode());
            return new ResponseEntity<>(bankRepository.save(updatedBank), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    public ResponseEntity<Bank> getBankById(long id) {
        Optional<Bank> bank = bankRepository.findById(id);
        if (bank.isPresent()) {
            return new ResponseEntity<>(bank.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<List<String>> listBanksNames() {
        try {
            List<String> banksNames = bankRepository.getAllBanksNames();
            return new ResponseEntity<>(banksNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    public ResponseEntity<Bank> addApplication(long bankId, Bank bank, long applicationId) {
//        Optional<Bank> bankData = bankRepository.findById(bankId);
//        Optional<Application> applicationData = applicationRepository.findById(applicationId);
//        if (bankData.isPresent() && applicationData.isPresent()) {
//            Bank updatedBank = bankData.get();
//            Application updatedApplication = applicationData.get();
//            updatedBank.addApplication(updatedApplication);
//            return new ResponseEntity<>(bankRepository.save(updatedBank), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }

}
